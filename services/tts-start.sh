#!/bin/bash

export PYTHONPATH=/home/dogu/catkin_ws/install_isolated/lib/python2.7/dist-packages:/opt/ros/melodic/lib/python2.7/dist-packages:/home/dogu/.local/lib/python3.6/site-packages:/home/dogu/.local/lib/python3.6/site-packages:/home/dogu/.local/lib/python3.6/site-packages/python3/dist-packages:/usr/lib/python3.6/dist-packages

/bin/sleep 40

bash -c "source /home/dogu/catkin_ws/devel_isolated/setup.bash && /opt/ros/melodic/bin/roslaunch dogu_tts_player start.launch"
