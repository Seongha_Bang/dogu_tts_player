# Dogu TTS Player


ROS기반 TTS 재생을 위한 노드


## TTS 모듈 설치


setup/TTSInstall.sh 실행
(TTS 모듈 자동 설치 스크립트)


## Catkin make
```
cd ~/catkin_ws
catkin_make_isolated --source src/dogu_tts_player
```


## Auto Launch Setting


```
cd dogu_tts_server
bash setup/autorun.sh
```


## 메시지 형태


- TTSTask.msg
  - string str: TTS로 재생할 문장
  - int64 delay: TTS 재생 간격 (재생 중에도 시간 측정됨)
  - int64 repeat: TTS 반복 횟수 (timer와 중복 사용 불가)
  - string job: TTS가 재생 되는 작업의 ID (timer와 중복 사용 불가)
  - int64 timer: 반복 재생할 총 시간 (repeat, job과 중복 사용 불가)


- JobID.msg
  - string id: 작업의 ID


## 토픽 설명


- play_tts\_task
  - TTSTask.msg 사용
  - TTS 재생


- stop_tts\_task
  - TTSTask.msg 사용
  - 해당하는 TTS 재생 취소
  - 메시지에 빈 값을 보낼 경우 모든 재생목록 취소


- job_stoped
  - JobID.msg 사용
  - 입력한 작업 ID를 가진 TTS 작업을 취소


- tts_now
  - TTSTask.msg 사용
  - 현재 재생 중인 TTS 작업 정보 출력 (10Hz)


## 사용


### 준비


필요한 모듈을 설치하기 위해 setup/TTSInstall.sh을 실행한다.


### 실행


start.launch 파일을 통해 실행한다.

추가적으로 필요한 설정은 없다.


### Topic - /play_tts\_task


TTS를 재생하려면 /play_tts\_task로 보내주면 된다.
단, repeat과 job이 사용될 경우에 timer는 값이 설정되지 않아야 하며,
반대로 timer가 사용될 경우에는 repeat과 job이 사용되면 안된다.


### Topic - /stop_tts\_task


만약 재생한 TTS를 종료하고 싶다면,
원하는 TTS 내용을 /stop_tts\_task로 보내주면 된다.
이 때, /play_tts\_task로 보냈던 내용과 /stop_tts\_task로 보내는 내용이 같아야 한다.

만약 메시지의 모든 항목에 값이 설정되지 않았다면 모든 TTS가 종료된다.

단, 종료한 TTS가 아직 재생중일 때는
재생이 끝날 때까지 기다렸다가 반복 없이 종료된다.


### Topic - /job_stoped


어떠한 작업이 종료되었을 때 TTS도 종료되어야 한다면   
TTS에 지정한 작업의 ID를 통해서 종료할 수 있다.

/job_stoped로 종료된 작업의 ID를 보내주면 해당 ID를 가진 TTS는 종료된다.

단, 종료한 TTS가 아직 재생중일 때는
재생이 끝날 때까지 기다렸다가 반복 없이 종료된다.


### Topic - /tts_now


현재 재생되고 있는 TTS 정보가 /tts_now를 통해 퍼블리시 되는데,
재생을 요청한 TTS 목록이 아닌, 실제로 스피커에서 재생 중인 TTS 정보를 보낸다.