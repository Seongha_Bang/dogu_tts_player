#!/usr/bin/env python

import rospy
from dogu_tts_player import *
from dogu_tts_player.msg import *
import os

class TTSServer:
	def __init__(self):
		tts_task_list = list()
		rospy.set_param('tts_task_list', tts_task_list)

	def play_tts_task(self, req):
		tts_task_list = rospy.get_param('tts_task_list')
		
		if (req.repeat != 0 or req.job != '') and req.timer != 0:
			raise Exception("3, 4 and 5 are duplicated")
		
		tts_task_list.append(TTSTaskList(req.str, req.delay, req.repeat, req.job, req.timer))
		
		rospy.set_param('tts_task_list', tts_task_list)

	def stop_tts_task(self, req):
		tts_task_list = rospy.get_param('tts_task_list')
		
		blank = req.str    == ''
		blank = req.delay  == 0  and blank
		blank = req.repeat == 0  and blank
		blank = req.job    == '' and blank
		blank = req.timer  == 0  and blank
		
		if blank:
			tts_task_list = list()
		else:
			for tts_task in tts_task_list:
				match = tts_task["string"] == req.str.decode('utf-8')
				match = tts_task["delay"] == req.delay   and match
				match = tts_task["repeat"] == req.repeat and match
				match = tts_task["job"] == req.job       and match
				match = tts_task["timer"] == req.timer   and match
				
				if match:
					tts_task_list.remove(tts_task)
		
		rospy.set_param('tts_task_list', tts_task_list)
	
	def job_stoped(self, req):
		tts_task_list = rospy.get_param('tts_task_list')
		
		for tts_task in tts_task_list:
			if tts_task["job"] == req.id:
				tts_task_list.remove(tts_task)
		
		rospy.set_param('tts_task_list', tts_task_list)

def tts_server():
	node = TTSServer()

	rospy.init_node('tts_server', anonymous=True)
	rospy.Subscriber('play_tts_task', TTSTask, node.play_tts_task)
	rospy.Subscriber('stop_tts_task', TTSTask, node.stop_tts_task)
	rospy.Subscriber('job_stopped', JobID, node.job_stoped)

	rospy.spin()

if __name__=='__main__':
	try:
		tts_server()
	except rospy.ROSInterruptException:
		pass
