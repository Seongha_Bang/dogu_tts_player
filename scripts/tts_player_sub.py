#!/usr/bin/env python

import rospy
import os
from threading import Thread

from dogu_tts_player.msg import TTSTask

os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = os.getcwd() + "/tts-key.json"

from google.cloud import texttospeech

from dogu_tts_player import *

now_playing = None


def play_tts(string):
    global now_playing

    client = texttospeech.TextToSpeechClient()

    synthesis_input = texttospeech.types.SynthesisInput(text=string)
    voice = texttospeech.types.VoiceSelectionParams(
        language_code="ko-KR", ssml_gender=texttospeech.enums.SsmlVoiceGender.MALE
    )
    audio_config = texttospeech.types.AudioConfig(
        audio_encoding=texttospeech.enums.AudioEncoding.MP3
    )

    response = client.synthesize_speech(synthesis_input, voice, audio_config)

    with open("output.mp3", "wb") as out:
        out.write(response.audio_content)

    playsound("output.mp3")
    now_playing = None

    if os.path.isfile("output.mp3"):
        os.remove("output.mp3")

def state_publisher():
    pub = rospy.Publisher('tts_now', TTSTask, queue_size=10)
    loop = rospy.Rate(10)

    while not rospy.is_shutdown():
        pub_data = TTSTask()
        tts_task_list = rospy.get_param('tts_task_list')

        for tts_task in tts_task_list:
            if tts_task['string'] == now_playing:
                pub_data.str = now_playing
                pub_data.delay = tts_task['delay']
                pub_data.repeat = tts_task['repeat']
                pub_data.job = tts_task['job']
                pub_data.timer = tts_task['timer']

        pub.publish(pub_data)

        loop.sleep()

def tts_client():
    global now_playing

    rospy.init_node('tts_client')

    pub_th = Thread(target=state_publisher, args=())
    pub_th.setDaemon(True)
    pub_th.start()

    loop = rospy.Rate(1)

    while not rospy.is_shutdown():
        try:
            tts_task_list = rospy.get_param('tts_task_list')
        except KeyError:
            return

        result = list()

        for tts_task in tts_task_list:
            if tts_task["timer"] != 0:
                skip = tts_task['timer_now'] >= tts_task['timer']
            else:
                skip = tts_task['repeat_now'] >= tts_task['repeat']

            if skip:
                continue

            if tts_task['delay_now'] >= tts_task['delay'] and now_playing == None:
                now_playing = tts_task["string"]
                t = Thread(target=play_tts, args=(tts_task["string"],))
                t.setDaemon(True)
                t.start()

                tts_task['delay_now'] = 0
                tts_task['repeat_now'] += 1

            tts_task['delay_now'] += 1
            tts_task['timer_now'] += 1

            result.append(tts_task)

        rospy.set_param('tts_task_list', result)

        loop.sleep()


if __name__ == '__main__':
    try:
        tts_client()
    except rospy.ROSInterruptException:
        pass
