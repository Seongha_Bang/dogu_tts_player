echo "[Setup Dogu TTS Service]"

# Copy script file
echo "[Copy start script file]"
bash -c "sudo cp services/tts-start.sh /home/"
echo "[Finish copy script file]"

# Copy service script file
echo "[Copy service file]"
bash -c "sudo cp services/dogu-tts.service /etc/systemd/system/"
echo "[Finish copy service file]"

# Enable service
bash -c "sudo systemctl enable dogu-tts.service"

bash -c "sudo systemctl start dogu-tts.service"
echo "[Finish setup]"