# Install google-cloud-storage
bash -c "pip install google-cloud-storage"

# Add URI for cloud SDK
echo "deb [signed-by=/usr/share/keyrings/cloud.google.gpg] https://packages.cloud.google.com/apt cloud-sdk main" | sudo tee -a /etc/apt/sources.list.d/google-cloud-sdk.list

# Install apt-transport-https
bash -c "sudo apt-get install apt-transport-https ca-certificates gnupg"

# Get public key about google cloud
bash -c "curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -"

# Install Google Cloud SDK
bash -c "sudo apt-get update && sudo apt-get install google-cloud-sdk"

# Install texttospeech
bash -c "pip install --upgrade google-cloud-texttospeech"

# Copy TTS Key JSON File
DIR=$(dirname $(realpath -s ${BASH_SOURCE}))
cp ${DIR}/tts-key.json ~/.ros/tts-key.json
