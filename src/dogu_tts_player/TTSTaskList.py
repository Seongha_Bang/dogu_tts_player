class TTSTaskList:
	def __getitem__(self, key):
		return getattr(self, key)

	def __setitem__(self, key, value):
		return setattr(self, key, value)

	def __init__(self, string, delay, repeat, job, timer):
		self.string     = string

		self.delay      = delay
		self.delay_now  = delay

		self.repeat     = repeat
		self.repeat_now = 0

		self.job        = job
		self.job_active = True

		self.timer      = timer
		self.timer_now  = 0
